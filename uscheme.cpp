// uscheme.cpp

#include <iostream>
#include <sstream>
#include <stack>
#include <string>
#include <unistd.h>
#include <ctype.h>

using namespace std;

// Globals ---------------------------------------------------------------------

bool BATCH = false;
bool DEBUG = false;

// Structures ------------------------------------------------------------------

struct Node {
    Node(string value1="", Node *left1=nullptr, Node *right1=nullptr){//Constructor for Node
        value = value1;
        left = left1;
        right = right1;
    }
    ~Node(){//Destructor recursively destroys tree
        if(left) delete left;
        if(right) delete right;
    }



    string value;
    Node * left;
    Node * right;

    friend ostream &operator<<(ostream &os, const Node &n);
};

ostream &operator<<(ostream &os, const Node &n) {//Outputs with correct format
	if(n.value == "")
		return os;
	os<< (n.value) << " ";
	os<< (n.left) << " ";
        os<< (n.right) << " ";
    return os;
}



// Parser ----------------------------------------------------------------------

string parse_token(istream &s) {//Get digits/parentheses/operators
    string token;
    char c1 = '\0';

    do{
    	c1 = s.get();
    }while(c1 == ' ');

    if(isdigit(c1)){//Digits
    	token += c1;
	c1 = s.peek();
    	while(isdigit(c1)){
            c1 = s.get();
	    token += c1;
	    c1 = s.peek();
        }
    }
    else{//Parentheses or operator
    	token = c1; 
    }

    return token;
}
Node *parse_expression(istream &s) {//Parse apart entire expression, call parse token appropriately

    Node *left = nullptr;
    Node *right = nullptr;
    string token = parse_token(s);

    if (token == "" || token == ")"){
    	return nullptr;
    }
    if (token == "("){
    	token = parse_token(s);
    	left = parse_expression(s);
    	if (left) right = parse_expression(s);
    	if (right) parse_token(s);
    }
    return new Node(token, left, right);
}

// Interpreter -----------------------------------------------------------------

void evaluate_r(const Node *n, stack<int> &s) {
	if(n->left)
		evaluate_r(n->left,s);//recursion
	if(n->right)
		evaluate_r(n->right,s);//recursion
	if(isdigit(n->value[0])){//if value is a number push to stack
		int temp = stoi(n->value);
		s.push(temp);
	}
	else if(n->value != "(" && n->value != ")"){//if value is a symbol, push to stack
		int num1, num2;
                
		num2 = s.top();
		s.pop();
		num1 = s.top();
		s.pop();
		if(n->value == "+"){//addition
			int added;
			added = num1+num2;
			s.push(added);
		}
		else if((n->value) == "-"){//subtraction
			int subtracted;
			subtracted = num1-num2;
			s.push(subtracted);
		}
		else if((n->value) == "*"){//multiplication
			int multiplied;
		        multiplied = num1*num2;
			s.push(multiplied);
		}
		else if((n->value) == "/"){//division
			int divided;
			divided = num1/num2;
			s.push(divided);
		}
	}
}
int evaluate(const Node *n) {
	stack<int> s;
	evaluate_r(n,s);
    return s.top();
}

// Main execution --------------------------------------------------------------

int main(int argc, char *argv[]) {
    string line;
    int c;

    while ((c = getopt(argc, argv, "bdh")) != -1) {
        switch (c) {
            case 'b': BATCH = true; break;
            case 'd': DEBUG = true; break;
            default:
                cerr << "usage: " << argv[0] << endl;
                cerr << "    -b Batch mode (disable prompt)"   << endl;
                cerr << "    -d Debug mode (display messages)" << endl;
                return 1;
        }
    }

    while (!cin.eof()) {//Take in input
        if (!BATCH) {
            cout << ">>> ";
            cout.flush();
        }

        if (!getline(cin, line)) {
            break;
        }

        if (DEBUG) { cout << "LINE: " << line << endl; }

        stringstream s(line);
        Node *n = parse_expression(s);
        if (DEBUG) { cout << "TREE: " << *n << endl; }

        cout << evaluate(n) << endl;

        delete n;//Delete root node
    }

    return 0;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
